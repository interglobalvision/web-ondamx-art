import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'
import startOfTomorrow from 'date-fns/start_of_tomorrow'
import endOfTomorrow from 'date-fns/end_of_tomorrow'
import { nowDate, nowSeconds, oneWeekFromNowSeconds } from '../src/lib/dates'
import 'babel-polyfill'

const preloadData = async (store,url) => {
   await store.firestore.get({
    collection: 'settings',
    doc: 'web',
    storeAs: 'settings-web',
  })

  // remove first "/"
  url = url.substring(1)

  const urlParts = url.split('/')

  if (urlParts.length === 0) { // HOME

  } else if (urlParts.length === 1) {
    const path = urlParts[0]
    switch(path) {
      case 'exposiciones':
        await store.firestore.get({
          collection: 'events',
          where: [
            ['status','==','published'],
            ['type', '==', 'exhibition'],
            ['closing', '>', nowDate()],
          ],
          storeAs: 'exhibitions',
        })
        break

      case 'eventos':
        await store.firestore.get({
          collection: 'events',
          orderBy: ['openingStart', 'asc'],
          where: [
            ['status', '==', 'published'],
            ['openingStart', '>=', startOfToday()],
            ['openingStart', '<=', endOfToday()],
          ],
          storeAs: 'events-today',
        })
        await store.firestore.get({
          collection: 'events',
          orderBy: ['openingStart', 'asc'],
          where: [
            ['status', '==', 'published'],
            ['openingStart', '>=', startOfTomorrow()],
            ['openingStart', '<=', endOfTomorrow()],
          ],
          storeAs: 'events-tomorrow',
        })
        await store.firestore.get({
          collection: 'events',
          where: [
            ['status','==','published'],
            ['openingStart', '>', endOfTomorrow()],
          ],
          storeAs: 'events',
        })
        await store.firestore.get({
          collection: 'events',
          orderBy: ['closing', 'asc'],
          where: [
            ['status', '==', 'published'],
            ['closing', '>=', nowDate()],
            ['multiDay', '==', true],
          ],
          storeAs: 'events-multiday',
        })
        break

      case 'revista':
        await store.firestore.get({
          collection: 'articles',
          orderBy: ['publishDate', 'desc'],
          where: [
            ['status','==','published'],
          ],
          storeAs: 'articles',
        })
        break
      default:
        break
    }
  } else if (urlParts.length === 2) {
    const path = urlParts[0]
    const itemId = urlParts[1]

    switch(path) {
      case 'evento':
        await store.firestore.get({
          collection: 'events',
          doc: itemId,
          storeAs: `event-${itemId}`
        })
        break
      case 'escrito':
        await store.firestore.get({
          collection: 'articles',
          doc: itemId,
          storeAs: `article-${itemId}`
        })
        break
      default:
        break
    }
  }

  return
}

export default preloadData
