import path from 'path'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { createReactAppExpress } from '@cra-express/core'
import { StaticRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { LocalizeProvider } from 'react-localize-redux'
import App from '../src/App'
import configureStore from '../src/lib/serverStore'
import { Helmet } from 'react-helmet'
import spanishTranslations from 'translations/es.translations.json'
import { renderToStaticMarkup } from 'react-dom/server';
import preloadData from './preload-data'
const clientBuildPath = path.resolve(__dirname, '../client')
import 'babel-polyfill'

let helmetContext
const initialState = {}
const store = configureStore(initialState)

async function handleUniversalRender(req, res) {
  const context = {}

  // Preload data based on URL
  await preloadData(store,req.url)

  const stream = (
    <Provider store={store}>
      <LocalizeProvider store={store} initialize={{
        languages: [
          { name: "Español", code: "es" },
          { name: "English", code: "en" }
        ],
        translation: spanishTranslations,
        options: {
          defaultLanguage: "es",
          renderToStaticMarkup: false
        }
      }}>
        <StaticRouter location={req.url} context={context}>
          <App />
        </StaticRouter>
      </LocalizeProvider>
    </Provider>
  )

  if (context.url) {
    res.redirect(301, context.url)
    return
  }

  return stream
}

const app = createReactAppExpress({
  clientBuildPath,
  // universalRender: (req, res) => <App />
  universalRender: handleUniversalRender,
  onFinish(req, res, html) {
    const state = store.getState()
    const helmet = Helmet.renderStatic()
    //const finalHtml = html.replace('<script src="/static/js/bundle.js"></script>', `<script>
    const finalHtml = html.replace('<script id="redux-state-preload"></script>', `<script>
      window.__PRELOADED_STATE__ = ${JSON.stringify(state).replace(/</g, '\\u003c')}
    </script>
    <script src="/static/js/bundle.js"></script>`)
      .replace('<div id="helmetTitle"></div>', helmet.title.toString())
      .replace('<div id="helmetMeta"></div>', helmet.meta.toString())
      .replace('<div id="helmetLink"></div>', helmet.link.toString())

    return res.send(finalHtml)
  }
})

if (module.hot) {
  module.hot.accept('../src/App', () => {
    App = require('../src/App').default
    console.log('✅ Server hot reloaded App')
  })
}

export default app
