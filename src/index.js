import React from 'react'
import ReactDOM from 'react-dom'
import ReactDOMServer from 'react-dom/server'
import { BrowserRouter } from 'react-router-dom'
import ScrollMemory from 'react-router-scroll-memory'
import { Provider } from 'react-redux'
import { LocalizeProvider } from 'react-localize-redux'
import store from 'lib/store'
import App from './App'
import * as serviceWorker from 'serviceWorker'

ReactDOM.hydrate(
  <Provider store={store}>
    <LocalizeProvider store={store}>
      <BrowserRouter>
        <ScrollMemory />
        <App />
      </BrowserRouter>
    </LocalizeProvider>
  </Provider>,
  document.getElementById('root')
)

console.log('NODE_ENV', process.env.NODE_ENV)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
