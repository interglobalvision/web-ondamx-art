import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Privacy from 'components/Privacy'

const PrivacyContainer = ({ settings }) => {
  return <Privacy settings={settings} />
}

PrivacyContainer.propTypes = {
  settings: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    settings: state.firestore.ordered['settings-web']
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'settings',
      doc: 'web',
      storeAs: 'settings-web',
    }
  ]),
  connect(mapStateToProps)
)(PrivacyContainer)
