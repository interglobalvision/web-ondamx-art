import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import endOfToday from 'date-fns/end_of_today'
import addDays from 'date-fns/add_days'

const ThisMonthContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'this-month'} translateId={'thisMonth'} />
}

ThisMonthContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    items: state.firestore.ordered['events-thismonth']
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['openingStart', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['openingStart', '>', addDays(endOfToday(), 6)],
        ['openingStart', '<=', addDays(endOfToday(), 29)],
      ],
      storeAs: 'events-thismonth',
    }
  ]),
  connect(mapStateToProps)
)(ThisMonthContainer)
