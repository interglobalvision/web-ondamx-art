import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Article from 'components/Article.js'

const ArticleContainer = ({ item, match }) => {
  const { itemId } = match.params
  return <Article item={item} itemId={itemId} />
}

const mapStateToProps = (state, props) => {
  const { itemId } = props.match.params

  return {
    item: state.firestore.data[`article-${itemId}`],
  }
}

export default compose(
  firestoreConnect(props => {
    const { itemId } = props.match.params

    return [{
      collection: 'articles',
      doc: itemId,
      storeAs: `article-${itemId}`
    }]
  }),
  connect(mapStateToProps)
)(ArticleContainer)
