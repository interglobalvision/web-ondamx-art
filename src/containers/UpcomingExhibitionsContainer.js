import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import { nowDate, nowSeconds } from 'lib/dates'

const UpcomingExhibitionsContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'upcoming-exhibitions'} translateId={'upcomingExhibitions'} />
}

UpcomingExhibitionsContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const events = state.firestore.ordered['exhibitions']
  let items = []

  if (events !== undefined) {
    items = events.slice().filter(item => item.openingStart.seconds > nowSeconds)
      .sort((a, b) => {
        // openingStart ASC
        return a.openingStart.seconds - b.openingStart.seconds
      })
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      where: [
        ['status','==','published'],
        ['type', '==', 'exhibition'],
        ['closing', '>', nowDate()],
      ],
      storeAs: 'exhibitions',
    }
  ]),
  connect(mapStateToProps)
)(UpcomingExhibitionsContainer)
