import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import { nowDate } from 'lib/dates'

const FeaturedContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'featured'} translateId={'featured'} />
}

FeaturedContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const events = state.firestore.ordered['events-featured']
  let items = []

  if (events !== undefined) {
    items = events.slice().sort((a, b) => {
      return a.featuredOrder - b.featuredOrder
    })
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      where: [
        ['highlight','==','featured'],
        ['status','==','published'],
        ['closing', '>', nowDate()],
      ],
      storeAs: 'events-featured',
    },
  ]),
  connect(mapStateToProps)
)(FeaturedContainer)
