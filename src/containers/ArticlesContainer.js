import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Articles from 'components/Articles'

const ArticlesContainer = ({ items }) => {
  return <Articles items={items} />
}

ArticlesContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    items: state.firestore.ordered['articles'],
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'articles',
      orderBy: ['publishDate', 'desc'],
      where: [
        ['status','==','published'],
      ],
      storeAs: 'articles',
    },
  ]),
  connect(mapStateToProps)
)(ArticlesContainer)
