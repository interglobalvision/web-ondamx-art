import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import About from 'components/About'

const AboutContainer = ({ settings }) => {
  return <About settings={settings} />
}

AboutContainer.propTypes = {
  settings: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    settings: state.firestore.ordered['settings-web']
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'settings',
      doc: 'web',
      storeAs: 'settings-web',
    }
  ]),
  connect(mapStateToProps)
)(AboutContainer)
