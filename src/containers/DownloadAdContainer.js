import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import DownloadAd from 'components/DownloadAd'

const DownloadAdContainer = ({ settings }) => {
  return <DownloadAd settings={settings} />
}

DownloadAdContainer.propTypes = {
  settings: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    settings: state.firestore.ordered['settings-web']
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'settings',
      doc: 'web',
      storeAs: 'settings-web',
    }
  ]),
  connect(mapStateToProps)
)(DownloadAdContainer)
