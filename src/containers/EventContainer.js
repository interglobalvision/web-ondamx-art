import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Event from 'components/Event'

const EventContainer = ({ item, match }) => {
  const { itemId } = match.params
  return <Event item={item} itemId={itemId} />
}

const mapStateToProps = (state, props) => {
  const { itemId } = props.match.params

  return {
    item: state.firestore.data[`event-${itemId}`],
  }
}

export default compose(
  firestoreConnect(props => {
    const { itemId } = props.match.params

    return [{
      collection: 'events',
      doc: itemId,
      storeAs: `event-${itemId}`
    }]
  }),
  connect(mapStateToProps)
)(EventContainer)
