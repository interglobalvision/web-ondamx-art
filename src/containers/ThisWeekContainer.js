import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import endOfToday from 'date-fns/end_of_today'
import addDays from 'date-fns/add_days'

const ThisWeekContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'this-week'} translateId={'thisWeek'} />
}

ThisWeekContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    items: state.firestore.ordered['events-thisweek']
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['openingStart', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['openingStart', '>', endOfToday()],
        ['openingStart', '<=', addDays(endOfToday(), 6)],
      ],
      storeAs: 'events-thisweek',
    }
  ]),
  connect(mapStateToProps)
)(ThisWeekContainer)
