import React from 'react'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router-dom'
import MainContent from 'components/MainContent.js'

const MainContainer = () => {
  return <MainContent />
}

export default withRouter(MainContainer)
