import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import { nowDate, nowSeconds } from 'lib/dates'

const UpcomingEventsContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'upcoming-events'} translateId={'upcomingEvents'} />
}

UpcomingEventsContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const events = state.firestore.ordered['events']
  let items = []

  if (events !== undefined) {
    items = events.slice().filter(item => {
        return item.openingStart.seconds > nowSeconds && item.type !== 'exhibition'
      }).sort((a, b) => {
        // openingStart ASC
        return a.openingStart.seconds - b.openingStart.seconds
      })
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      where: [
        ['status','==','published'],
        ['closing', '>', nowDate()],
      ],
      storeAs: 'events',
    }
  ]),
  connect(mapStateToProps)
)(UpcomingEventsContainer)
