import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import { nowDate } from 'lib/dates'

const HighlightsContainer = ({ featured, highlights }) => {
  return (
    <>
      <EventsList items={featured} sectionId={'featured'} translateId={'highlights'} />
      <EventsList items={highlights} sectionId={'highlighted'} />
    </>
  )
}

HighlightsContainer.propTypes = {
  featured: PropTypes.array,
  highlights: PropTypes.array
}

const mapStateToProps = (state, props) => {
  let featured = state.firestore.ordered['events-featured']
  let highlights = state.firestore.ordered['events-highlight']

  if (featured !== undefined) {
    featured = featured.slice().sort((a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    })
  }

  if (highlights !== undefined) {
    highlights = highlights.slice().sort((a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    })
  }

  return {
    featured,
    highlights
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['closing', 'desc'],
      where: [
        ['highlight','==','featured'],
        ['status','==','published'],
        ['closing', '>', nowDate()],
      ],
      storeAs: 'events-featured',
    },
    {
      collection: 'events',
      where: [
        ['highlight','==','highlighted'],
        ['status','==','published'],
        ['closing', '>', nowDate()],
      ],
      orderBy: ['closing', 'desc'],
      storeAs: 'events-highlight',
    }
  ]),
  connect(mapStateToProps)
)(HighlightsContainer)
