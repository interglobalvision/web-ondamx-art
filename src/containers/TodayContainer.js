import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'
import { nowDate, nowSeconds } from 'lib/dates'

const TodayContainer = ({ items }) => {
  return <EventsList items={items} sectionId={'today'} translateId={'today'} />
}

TodayContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const today = state.firestore.ordered['events-today']
  const multiday = state.firestore.ordered['events-multiday']
  let items = []

  if (today !== undefined || multiday !== undefined) {
    if (today !== undefined) {
      items.push.apply(items, today)
    }

    if (multiday !== undefined) {
      items.push.apply(items, multiday.filter((item) => {
        return item.openingStart.seconds < nowSeconds && item.type !== 'exhibition'
      }))
    }

    items = items.sort((a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    })
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['openingStart', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['openingStart', '>=', startOfToday()],
        ['openingStart', '<=', endOfToday()],
      ],
      storeAs: 'events-today',
    },
    {
      collection: 'events',
      orderBy: ['closing', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['closing', '>=', nowDate()],
        ['multiDay', '==', true],
      ],
      storeAs: 'events-multiday',
    }
  ]),
  connect(mapStateToProps)
)(TodayContainer)
