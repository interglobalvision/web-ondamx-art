import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'

const ItemsContainer = ({ Component, items, sectionId, translateId, collection }) => {
  return <Component items={items} sectionId={sectionId} translateId={translateId} />
}

ItemsContainer.propTypes = {
  items: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  const { firestoreQuery, filterCallback, sortCompare, limit } = props
  let items = []

  firestoreQuery.forEach(query => {
    const queryItems = state.firestore.ordered[query.storeAs]

    if (queryItems !== undefined) {
      if (filterCallback) {
        const filteredItems = queryItems.filter(filterCallback)
        items.push.apply(items, filteredItems)
      } else {
        items.push.apply(items, queryItems)
      }
    }
  })

  if (sortCompare) {
    items.sort(sortCompare)
  }

  if (limit) {
    items = items.slice(0, limit)
  }

  return {
    items
  }
}

export default compose(
  firestoreConnect(props => {
    return props.firestoreQuery
  }),
  connect(mapStateToProps)
)(ItemsContainer)
