import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Exhibitions from 'components/Exhibitions'
import { nowDate, nowSeconds, oneWeekFromNowSeconds } from 'lib/dates'

const ExhibitionsContainer = ({ ongoing, upcoming }) => {
  return <Exhibitions ongoing={ongoing} upcoming={upcoming} />
}

ExhibitionsContainer.propTypes = {
  ongoing: PropTypes.array,
  upcoming: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  let ongoing = []
  let upcoming = []
  const items = state.firestore.ordered['exhibitions']

  if (items !== undefined) {
    ongoing = items.filter(item => item.openingStart.seconds < nowSeconds)
      .sort((a, b) => {
        // closing ASC
        return a.closing.seconds - b.closing.seconds
      })
    if (ongoing.length) {
      // filter out duplicates
      ongoing = Array.from(new Set(ongoing.map(i => i.id))).map(id => ongoing.find(i => i.id === id))
    }

    upcoming = items.filter(item => item.openingStart.seconds > nowSeconds)
      .sort((a, b) => {
        // openingStart ASC
        return a.openingStart.seconds - b.openingStart.seconds
      })
    if (upcoming.length) {
      // filter out duplicates
      upcoming = Array.from(new Set(upcoming.map(i => i.id))).map(id => upcoming.find(i => i.id === id))
    }
  }

  return {
    ongoing,
    upcoming,
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      where: [
        ['status','==','published'],
        ['type', '==', 'exhibition'],
        ['closing', '>', nowDate()],
      ],
      storeAs: 'exhibitions',
    },
  ]),
  connect(mapStateToProps)
)(ExhibitionsContainer)
