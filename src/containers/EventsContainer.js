import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import Events from 'components/Events'
import startOfToday from 'date-fns/start_of_today'
import endOfToday from 'date-fns/end_of_today'
import startOfTomorrow from 'date-fns/start_of_tomorrow'
import endOfTomorrow from 'date-fns/end_of_tomorrow'
import { nowDate, nowSeconds, oneWeekFromNowSeconds } from 'lib/dates'

const EventsContainer = ({ today, tomorrow, upcoming }) => {
  return <Events today={today} tomorrow={tomorrow} upcoming={upcoming} />
}

EventsContainer.propTypes = {
  today: PropTypes.array,
  tomorrow: PropTypes.array,
  upcoming: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  let today = state.firestore.ordered['events-today'] !== undefined ? state.firestore.ordered['events-today'].slice() : []
  let tomorrow = state.firestore.ordered['events-tomorrow'] !== undefined ? state.firestore.ordered['events-tomorrow'].slice() : []
  const events = state.firestore.ordered['events']
  const multiday = state.firestore.ordered['events-multiday']

  let upcoming = []

  if (today !== undefined && multiday !== undefined) {

    if (multiday.length > 0) {
      today.push.apply(today, multiday.filter((item) => {
        if (item.type === 'exhibition' && !item.hasOpening) {
          return false
        }
        return item.openingStart.seconds < nowSeconds
      }))
    }
    today.sort((a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    })
    if (today.length) {
      // filter out duplicates
      today = Array.from(new Set(today.map(i => i.id))).map(id => today.find(i => i.id === id))
    }
  }

  if (tomorrow !== undefined && multiday !== undefined) {
    if (multiday.length > 0) {
      tomorrow.push.apply(tomorrow, multiday.filter((item) => {
        if (item.type === 'exhibition' && !item.hasOpening) {
          return false
        }
        return item.openingStart.seconds <= (endOfToday().getTime() / 1000) && item.closing.seconds >= (endOfToday().getTime() / 1000)
      }))
    }
    tomorrow.sort((a, b) => {
      return b.openingStart.seconds - a.openingStart.seconds
    })
    if (tomorrow.length) {
      // filter out duplicates
      tomorrow = Array.from(new Set(tomorrow.map(i => i.id))).map(id => tomorrow.find(i => i.id === id))
    }
  }

  if (events !== undefined) {
    upcoming = events
      .filter((item) => {
        return item.openingStart.seconds > nowSeconds && item.type !== 'exhibition'
      })
      .sort((a, b) => {
        // openingStart ASC
        return a.openingStart.seconds - b.openingStart.seconds
      })
    if (upcoming.length) {
      // filter out duplicates
      upcoming = Array.from(new Set(upcoming.map(i => i.id))).map(id => upcoming.find(i => i.id === id))
    }
  }

  return {
    today,
    tomorrow,
    upcoming,
  }
}

export default compose(
  firestoreConnect([
    {
      collection: 'events',
      orderBy: ['openingStart', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['openingStart', '>=', startOfToday()],
        ['openingStart', '<=', endOfToday()],
      ],
      storeAs: 'events-today',
    },
    {
      collection: 'events',
      orderBy: ['openingStart', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['openingStart', '>=', startOfTomorrow()],
        ['openingStart', '<=', endOfTomorrow()],
      ],
      storeAs: 'events-tomorrow',
    },
    {
      collection: 'events',
      where: [
        ['status','==','published'],
        ['openingStart', '>', endOfTomorrow()],
      ],
      storeAs: 'events',
    },
    {
      collection: 'events',
      orderBy: ['closing', 'asc'],
      where: [
        ['status', '==', 'published'],
        ['closing', '>=', nowDate()],
        ['multiDay', '==', true],
      ],
      storeAs: 'events-multiday',
    }
  ]),
  connect(mapStateToProps)
)(EventsContainer)
