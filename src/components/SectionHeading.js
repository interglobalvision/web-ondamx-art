import React from 'react'
import { Translate } from 'react-localize-redux'

const SectionHeading = ({ translateId }) => {
  return (
    <h2 className='padding-top-small padding-bottom-small font-heading font-size-large'><Translate id={translateId} /></h2>
  )
}

export default SectionHeading
