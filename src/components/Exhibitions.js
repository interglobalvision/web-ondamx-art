import React from 'react'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import DownloadAdContainer from 'containers/DownloadAdContainer'
import Meta from 'components/Meta'
import { withTranslate } from 'lib/translations'

const Exhibitions = ({ ongoing, upcoming, translate }) => {

  return (
    <>
      <Meta title={translate('exhibitions')} description={translate('exhibitionsDescription')} />
      <EventsList items={ongoing} sectionId={'ongoing'} translateId={'ongoing'} />

      <EventsList items={upcoming} sectionId={'upcoming'} translateId={'upcoming'} />

      <div className='container grid-row padding-bottom-small'>
        <div className='grid-item item-s-12'><div className='border'></div></div>
      </div>

      {/*}<section id='banner-ads'>
        <div className='container'>
          <div className='grid-row'>
            <div className='grid-item item-s-12 item-l-4 text-align-center margin-bottom-small'>
              Avertisement
            </div>
            <div className='grid-item item-s-12 item-l-4 text-align-center margin-bottom-small'>
              Avertisement
            </div>
            <div className='grid-item item-s-12 item-l-4 text-align-center margin-bottom-small'>
              Avertisement
            </div>
          </div>
        </div>
      </section>

      <div className='container grid-row padding-bottom-small'>
        <div className='grid-item item-s-12'><div className='border'></div></div>
      </div>*/}

      <DownloadAdContainer />
    </>
  )
}

export default withTranslate(Exhibitions)
