import React from 'react'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import ArticleItem from 'components/ArticleItem'
import SectionHeading from 'components/SectionHeading'

const ArticlesMobile = ({items}) => {

  if (!isLoaded(items)) {
    return null
  }

  if (isEmpty(items)) {
    return null
  }

  return (
    <section id='articles' className='padding-bottom-small background-blue-light'>
      <div className='container'>
        <div className='grid-row'>
          <div className='grid-item item-s-12'>
            <SectionHeading translateId={'magazine'} />
          </div>
          {items.map((item, index)=> {
            return (
              <ArticleItem key={item.id} item={item} />
            )
          })}
        </div>
      </div>
    </section>
  )
}

export default ArticlesMobile
