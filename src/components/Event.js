import React from 'react'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent, withTranslate } from 'lib/translations'
import { getMediaThumb } from 'lib/images'
import { getDraftHtml } from 'lib/utils'
import { formatEventDates } from 'lib/dates'
import { getStaticMapUrl } from 'lib/geoloc'
import { isMobileOnly, isMobile } from 'react-device-detect'
import MobileShare from 'components/MobileShare'
import SpaceDetails from 'components/SpaceDetails'
import Meta from 'components/Meta'

const Event = ({ item, activeLanguage, translate, itemId }) => {
  if (!isLoaded(item)) {
    return null
  }

  if (isEmpty(item)) {
    return null
  }

  const content = flattenLocalizedContent(item, activeLanguage.code)
  const mainContent = getDraftHtml(item, activeLanguage.code)

  const { location: { lat, lon } } = content
  const mapSize = 150
  const mapUrl = getStaticMapUrl(lat, lon, mapSize * 1.5, mapSize * 1.5)

  return (
    <>
      <Meta
        title={content.seoTitle !== '' ? content.seoTitle : content.title}
        description={content.seoDescription !== '' ? content.seoDescription : content.subtitle}
        image={content.coverImage}
      />
      {!isMobileOnly &&
        <header className='padding-top-small padding-bottom-small'>
          <div className='container'>
            <div className='grid-row justify-center'>
              <div className='grid-item item-s-10 padding-bottom-small'>
                <span className='font-heading font-grey font-size-mid'><Translate id={content.type} /></span>
              </div>
              <div className='grid-item item-s-12'>
                <h1 className='font-bold font-size-extra'>{content.name}</h1>
              </div>
            </div>
          </div>
        </header>
      }
      <section>
        {isMobileOnly ? (
          <div className='cover-holder' style={{
            backgroundImage: `url(${content.coverImage.mediaUrl})`
          }}></div>
        ) : (
          <div className='container'>
            <div className='grid-row justify-center'>
              <div className='grid-item item-s-12 item-l-10'>
                <div className='cover-holder' style={{
                  backgroundImage: `url(${content.coverImage.mediaUrl})`
                }}></div>
              </div>
            </div>
          </div>
        )}
      </section>
      <section>
        <div className='container'>
          <div className='grid-row justify-center margin-bottom-small'>
            <div className='grid-item item-s-12 only-mobile text-align-right margin-top-small'>
              {isMobile &&
                <MobileShare
                  title={content.name}
                  slug={item.slug}
                  type={'evento'}
                  itemId={itemId}
                />
              }
            </div>
            <div id='event-details' className='item-s-12 item-m-4 item-l-3 margin-top-small'>
              <div className='grid-row'>
                <div className='grid-item'>
                  {isMobileOnly &&
                    <header>
                      <h1 id='event-name-mobile' className='font-bold font-size-large'>{content.name}</h1>
                    </header>
                  }
                  <div>
                    <span className={isMobileOnly ? 'font-size-mid' : 'font-size-mid font-bold'}>{content.title}</span>
                  </div>
                  <div>
                    <span className={isMobileOnly ? 'font-uppercase font-grey font-size-micro' : 'font-uppercase font-grey'}><Translate id={content.type} /></span>
                  </div>
                  <div>
                    {formatEventDates(content, activeLanguage.code, translate)}
                  </div>
                  {content.ticketUrl.length > 0 &&
                    <div>
                      <a href={content.ticketUrl}><Translate id='buyTickets' /></a>
                    </div>
                  }
                </div>
              </div>
              <div id='space-details-holder' className='grid-row justify-between'>
                <div className='margin-top-small grid-item'>
                  <div>
                    <span className='font-size-mid font-bold margin-bottom-micro'>{content.space.name}</span>
                  </div>
                  <SpaceDetails
                    content={content}
                    translate={translate}
                    space={content.space}
                    showAddress
                    showHours
                    showPhone
                    showWebsite
                    showEmail
                    showInstagram
                  />
                </div>
                <div className='margin-top-small grid-item'>
                  <a id='event-map-holder' href={`https://www.google.com/maps/place/${lat},${lon}`}><img src={mapUrl} /></a>
                </div>
              </div>
            </div>
            <div id='event-content' className='grid-item item-s-12 item-m-8 item-l-7 margin-top-small'>
              <div id='content-holder'>
                {!mainContent.isSameLanguage &&
                  <div className='padding-bottom-small font-grey'><span><Translate id='languageMissing' /></span></div>
                }
                <div dangerouslySetInnerHTML={{__html: mainContent.contentHtml}} />
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default compose(
  withLocalize,
  withTranslate,
)(Event)
