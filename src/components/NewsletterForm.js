import React, { useState } from 'react'
import { Translate } from 'react-localize-redux'
import { withTranslate } from 'lib/translations'
//import axios from 'axios'
import jsonp from 'jsonp'
import toQueryString from 'to-querystring'

const NewsletterForm = ({translate}) => {
  const [values, setValues] = useState({FNAME: '', LNAME: '', EMAIL: ''})
  const [status, setStatus] = useState(null)
  const [message, setMessage] = useState(null)

  const handleChange = e => {
    setStatus(null)
    setMessage(null)
    const {name, value} = e.target
    setValues({...values, [name]: value})
  }

  const handleSubmit = e => {
    e.preventDefault()

    const params = toQueryString(values);

    // replaced '/post?' with '/post-json?'
    //const url = 'https://vision.us18.list-manage.com/subscribe/post-json?u=cb99113ca0539c919793889f9&amp;id=1418a59b19&' + params
    const url = 'https://art.us17.list-manage.com/subscribe/post-json?u=e0c8e2af931b348ed70593c73&amp;id=1a95a45161&' + params

    setStatus('sending')

    jsonp(
      url,
      {
        param: 'c'
      },
      (err, data) => {
        if (err) {
          setStatus('error')
          setMessage(translate('mailchimpServerError'))
        } else if (data.result === 'success' || data.msg.includes('is already subscribed to list')) {
          setStatus('success')
          setMessage(translate('mailchimpSuccess'))
          setValues({FNAME: '', LNAME: '', EMAIL: ''})
        } else {
          setStatus('error')
          setMessage(translate('mailchimpValidationError'))
        }
      }
    )
  }

  return (
    <form id='newsletter-form' className='grid-row justify-end'>
      <div className='grid-item item-s-12 margin-bottom-small'><span><Translate id='subscribeToNewsletter' /></span></div>
      <div className='grid-item item-s-6 item-l-5 margin-bottom-tiny'>
        <input
          type='text'
          name='FNAME'
          autoCapitalize='off'
          autoCorrect='off'
          placeholder={translate('firstName')}
          size='25'
          value={values.firstName}
          onChange={handleChange}
        />
      </div>
      <div className='grid-item item-s-6 item-l-7 margin-bottom-tiny'>
        <input
          type='text'
          name='LNAME'
          autoCapitalize='off'
          autoCorrect='off'
          placeholder={translate('lastName')}
          size='25'
          value={values.lastName}
          onChange={handleChange}
        />
      </div>
      <div className='grid-item item-s-12 margin-bottom-tiny'>
        <input
          type='email'
          name='EMAIL'
          autoCapitalize='off'
          autoCorrect='off'
          placeholder='Email'
          size='25'
          value={values.email}
          onChange={handleChange}
        />
      </div>
      <div className='grid-item item-s-7 item-m-8 item-l-9'>
        {message !== null &&
          <div dangerouslySetInnerHTML={{ __html: message }} />
        }
      </div>
      <div className='grid-item item-s-5 item-m-4 item-l-3'>
        <button
          onClick={handleSubmit}
          disabled={status === 'sending'}
        >
          <Translate id={status === 'sending' ? 'sending' : 'join'} />
        </button>
      </div>
    </form>
  )
}

export default withTranslate(NewsletterForm)
