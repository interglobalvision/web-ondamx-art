import React from 'react'
import { Link } from 'react-router-dom'
import { Translate } from 'react-localize-redux'
import DownloadAdContainer from 'containers/DownloadAdContainer'

const HomeMobile = () => {
  return (
    <>
      <section>
        <nav className='grid-column font-heading'>
          <Link to='/destacados' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='highlights' />
          </Link>
          <Link to='/actualmente' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='ongoingExhibitions' />
          </Link>
          <Link to='/eventos' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='upcomingEvents' />
          </Link>
          <Link to='/ultimos-dias' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='closingSoon' />
          </Link>
          <Link to='/proximas-exposiciones' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='upcomingExhibitions' />
          </Link>
          <Link to='/revista' className='home-mobile-item grid-item padding-top-tiny padding-bottom-tiny'>
            <Translate id='magazine' />
          </Link>
        </nav>
      </section>
      <div className='padding-top-small'>
        <DownloadAdContainer />
      </div>
    </>
  )
}

export default HomeMobile
