import React from 'react'
import { compose } from 'redux'
import PropTypes from 'prop-types'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent } from 'lib/translations'
import { getMediaThumb } from 'lib/images'
import { getDraftHtml } from 'lib/utils'
import MobileShare from 'components/MobileShare'
import ArticleDate from 'components/ArticleDate'
import Meta from 'components/Meta'
import { isMobile } from 'react-device-detect'

const Article = ({ item, activeLanguage, itemId }) => {

  if (!isLoaded(item)) {
    return null
  }

  if (isEmpty(item)) {
    return null
  }

  const content = flattenLocalizedContent(item, activeLanguage.code)
  const mainContent = getDraftHtml(item, activeLanguage.code)

  return (
    <>
      <Meta
        title={content.seoTitle !== '' ? content.seoTitle : content.title}
        description={content.seoDescription !== '' ? content.seoDescription : content.subtitle}
        type='article'
        image={content.coverImage}
      />
      <header className='background-blue-light padding-top-small padding-bottom-small'>
        <div className='container'>
          <div className='grid-row'>
            <div className='grid-item item-s-12'>
              <div className='grid-row justify-between'>
                <div className='not-mobile'><span className='font-heading font-size-mid'><Translate id={content.type} /></span></div>
                <div className='margin-bottom-small'><span><Translate id='publishedOn' /> <ArticleDate timestamp={content.publishDate.seconds} currentLanguage={activeLanguage.code}/></span></div>
              </div>
              <h1 className='font-bold font-size-extra margin-bottom-small'>{content.title}</h1>
            </div>
            {content.subtitle !== '' &&
              <div className='grid-item item-s-12 only-mobile margin-bottom-basic'>
                <span className='font-size-mid font-bold'>{content.subtitle}</span>
              </div>
            }
            <div className='grid-item item-s-12 only-mobile margin-bottom-micro'><span className='font-uppercase'><Translate id={content.type} /></span></div>
            {content.author !== '' &&
              <div className='grid-item item-s-12 margin-bottom-small'>
                <span><Translate id='by' /> {content.author}</span>
              </div>
            }
            {content.subtitle !== '' &&
              <div className='grid-item item-s-6 offset-s-3 not-mobile'>
                <span className='font-size-mid'>{content.subtitle}</span>
              </div>
            }
            {isMobile &&
              <div className='grid-item'>
                <MobileShare
                  title={content.title}
                  slug={item.slug}
                  type={'escrito'}
                  itemId={itemId}
                />
              </div>
            }
          </div>
        </div>
      </header>
      {content.coverImage !== null &&
        <section>
          <div className='cover-holder' style={{
            backgroundImage: `url(${content.coverImage.mediaUrl})`
          }}></div>
        </section>
      }
      <section>
        <div className='container'>
          <div className='grid-row justify-center'>
            <div className='grid-item item-s-12 item-m-10 padding-top-small padding-bottom-basic grid-row justify-center'>
              <div id='content-holder'>
                {!mainContent.isSameLanguage &&
                  <div className='padding-bottom-small font-grey'><span><Translate id='languageMissing' /></span></div>
                }
                <div id='article-content' dangerouslySetInnerHTML={{__html: mainContent.contentHtml}} />
                <div className='text-align-right'>
                {isMobile ? (
                  <MobileShare
                    title={content.title}
                    slug={item.slug}
                    type={'escrito'}
                    itemId={itemId}
                  />
                ) : (
                  <span><Translate id='publishedOn' /> <ArticleDate timestamp={content.publishDate.seconds} currentLanguage={activeLanguage.code}/></span>
                )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default compose(
  withLocalize
)(Article)
