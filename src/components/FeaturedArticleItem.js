import React from 'react'
import { compose } from 'redux'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent } from 'lib/translations'
import { trimByChar, constructUrlSlug } from 'lib/utils'
import { getMediaThumb } from 'lib/images'
import ReadMore from 'components/ReadMore'

const ArticleFeaturedItem = ({ item, index, activeLanguage }) => {
  const content = flattenLocalizedContent(item, activeLanguage.code)
  const contentState = JSON.parse(content.mainContent)
  const firstParagraph = contentState.blocks[0].text
  const thumbUrl = content.coverImage ? getMediaThumb(content.coverImage.mediaUrl, 581, 459) : null
  const reverse = index % 2
  const urlSlug = constructUrlSlug(item.slug, content.title)

  return (
    <div key={content.id}>
      <div className='container'>
        <Link to={`/escrito/${urlSlug}-${item.id}`}>
          <div className={'grid-row padding-top-small align-items-end ' + (reverse ? 'row-reverse text-align-right' : '')}>
            <div className='grid-item item-s-6'>
              <img src={thumbUrl} alt={content.title} />
            </div>
            <div className='grid-item item-s-6'>
              <h3 className='font-bold margin-bottom-tiny font-size-large'>{content.title}</h3>
              {content.subtitle !== '' &&
                <div className='margin-bottom-tiny font-size-mid'>{content.subtitle}</div>
              }
              <div className='margin-bottom-tiny font-size-tiny'>
                <span>{trimByChar(firstParagraph)} <span className='u-inline-block'><ReadMore /></span></span>
              </div>
              <div className={'grid-row font-size-micro ' + (reverse ? 'row-reverse' : '')}>
                <div className='item-s-4 font-uppercase'><Translate id={content.type} /></div>
                <div className='item-s-4'><Translate id='by' /> {content.author}</div>
              </div>
            </div>
            <div className='grid-item item-s-12 padding-top-small'><div className='border'></div></div>
          </div>
        </Link>
      </div>
    </div>
  )
}

export default compose(
  withLocalize,
)(ArticleFeaturedItem)
