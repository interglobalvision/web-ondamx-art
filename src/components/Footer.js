import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import NewsletterForm from 'components/NewsletterForm'

const Footer = ({ activeLanguage, setActiveLanguage }) => {
  const languageCode = activeLanguage !== undefined ? activeLanguage.code : 'es'

  const setLanguage = (event) => {
    setActiveLanguage(event.target.value)
    localStorage.setItem('language', event.target.value)
  }

  return (
    <footer id='footer' className='background-blue-onda font-white padding-top-basic padding-bottom-mid font-size-small'>
      <div className='container'>
        <div className='only-mobile'>
          <div className='grid-column'>
            <div className='grid-item grid-row justify-around'>
              <a href='https://apps.apple.com/cl/app/onda-mx/id1473217717' className='store-button'>
                <img src={`/appstore-${languageCode}.png`} />
              </a>
              <a href='https://play.google.com/store/apps/details?id=com.interglobalvision.ondamx&hl=es' className='store-button'>
                <img src={`/googleplay-${languageCode}.png`} />
              </a>
            </div>
            <div className='grid-item text-align-center margin-top-basic'>
              <label>
                <div className='margin-bottom-small'><Translate id='language' /></div>
                {activeLanguage !== undefined &&
                  <select id='footer-language-select' className='font-size-mid' value={activeLanguage.code} onChange={setLanguage}>
                    <option value="es">Español</option>
                    <option value="en">English</option>
                  </select>
                }
              </label>
            </div>
            <div className='grid-row justify-around margin-top-basic'>
              <div className='grid-item'><Link to='/acerca-de' className='link-underline'><Translate id='about' /></Link></div>
              <div className='grid-item'><Link to='/support' className='link-underline'><Translate id='support' /></Link></div>
            </div>
            <div className='grid-row justify-around margin-top-small'>
              <div className='grid-item'><Link to='/terminos-y-condiciones' className='link-underline'><Translate id='terms' /></Link></div>
              <div className='grid-item'><Link to='/privacidad' className='link-underline'><Translate id='privacy' /></Link></div>
            </div>
            <div className='margin-top-basic'>
              <NewsletterForm />
            </div>
            <div className='margin-top-mid font-size-tiny text-align-center'><span>&copy; Onda MXd {(new Date().getFullYear())}</span></div>
          </div>
        </div>

        <div className='not-mobile'>
          <div className='grid-row justify-between'>
            <div className='grid-item item-m-12 margin-bottom-basic'>
              <Link to='/'><img id='logo-footer' src='/logo-onda-white.png' /></Link>
            </div>
            <div className='item-m-6 grid-column'>
              <div className='grid-item margin-bottom-micro grid-row align-items-center'><img src="/icon-email.png" className='contact-icon' /><a href='mailto:info@ondamx.art'>info@ondamx.art</a></div>
              <div className='grid-item margin-bottom-small grid-row align-items-center'><img src="/icon-instagram.png" className='contact-icon' /><a href='https://www.instagram.com/onda_mx/'>@onda_mx</a></div>
              <div className='grid-item grid-row'>
                <a href='https://apps.apple.com/mx/app/onda-mx/id1473217717' className='store-button'>
                  <img src={`/appstore-${languageCode}.png`} />
                </a>
                <a href='https://play.google.com/store/apps/details?id=com.interglobalvision.ondamx&hl=es' className='store-button'>
                  <img src={`/googleplay-${languageCode}.png`} />
                </a>
              </div>
            </div>
            <div className='item-m-6'>
              <NewsletterForm />
            </div>
            <div className='grid-item item-m-12 padding-top-small padding-bottom-tiny'><div className='border'></div></div>
            <div className='item-m-12 grid-row justify-between font-size-tiny'>
              <div className='grid-row justify-center'>
                <div className='grid-item'><Link to='/acerca-de' className='link-underline'><Translate id='about' /></Link></div>
                <div className='grid-item'><a href='mailto:help@ondamx.art' className='link-underline'><Translate id='support' /></a></div>
                <div className='grid-item'><Link to='/terminos-y-condiciones' className='link-underline'><Translate id='terms' /></Link></div>
                <div className='grid-item'><Link to='/privacidad' className='link-underline'><Translate id='privacy' /></Link></div>
              </div>
              <div className='grid-item'><span>&copy; Onda MX {(new Date().getFullYear())}</span></div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default withLocalize(Footer)
