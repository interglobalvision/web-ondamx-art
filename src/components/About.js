import React from 'react'
import { compose } from 'redux'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent, withTranslate } from 'lib/translations'
import { getDraftHtml } from 'lib/utils'
import SectionHeading from 'components/SectionHeading'
import Meta from 'components/Meta'
import { Link } from 'react-router-dom'

const About = ({ settings, activeLanguage, translate }) => {
  if (!isLoaded(settings)) {
    return null
  }

  if (isEmpty(settings)) {
    return null
  }

  const content = flattenLocalizedContent(settings[0], activeLanguage.code)
  const aboutContent = getDraftHtml(settings[0], activeLanguage.code, 'aboutContent')
  const aboutCredits = getDraftHtml(settings[0], activeLanguage.code, 'aboutCredits')

  return (
    <section id='about'>
      <Meta
        title={translate('about')}
        description={content.aboutHeadline}
        image={content.aboutImage}
      />
      <div className='container'>
        <div className='grid-row margin-bottom-basic'>
          <div className='grid-item item-m-4 offset-l-1'>
            {content.mockupImage !== undefined && content.mockupImage !== null &&
              <div className='mockup-holder' style={{
                backgroundImage: `url(${content.mockupImage.mediaUrl})`
              }}></div>
            }
          </div>
          <div className='grid-item item-m-8 item-l-6'>
            {content.aboutHeadline !== undefined &&
              <h2 className='margin-bottom-tiny font-heading font-blue font-size-extra'>{content.aboutHeadline}</h2>
            }
            <div className='margin-bottom-small' dangerouslySetInnerHTML={{__html: aboutContent.contentHtml }} />
            <div className='grid-row justify-center'>
              <a href='https://apps.apple.com/mx/app/onda-mx/id1473217717' className='store-button'>
                <img src={`/appstore-${activeLanguage.code}.png`} />
              </a>
              <a href='https://play.google.com/store/apps/details?id=com.interglobalvision.ondamx&hl=es' className='store-button'>
                <img src={`/googleplay-${activeLanguage.code}.png`} />
              </a>
            </div>
          </div>
        </div>
      </div>
      {content.aboutImage &&
        <div className='cover-holder' style={{
          backgroundImage: `url(${content.aboutImage.mediaUrl})`
        }}><img src={content.aboutImage.mediaUrl} alt={content.aboutHeadline} className='u-visuallyhidden'/></div>
      }
      <div className='container'>
        <div className='grid-row margin-bottom-small'>
          <div className='grid-item item-m-12 item-l-10 offset-l-1'>
            <SectionHeading translateId={'credits'} />
          </div>
          <div className='grid-item item-m-12 item-l-10 offset-l-1'>
            <div dangerouslySetInnerHTML={{__html: aboutCredits.contentHtml }} />
          </div>
        </div>
      </div>
    </section>
  )
}

export default compose(
  withLocalize,
  withTranslate
)(About)
