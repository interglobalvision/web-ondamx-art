import React from 'react'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import EventsList from 'components/EventsList'
import DownloadAdContainer from 'containers/DownloadAdContainer'
import AdSense from 'react-adsense'
import Meta from 'components/Meta'
import { withTranslate } from 'lib/translations'

const Events = ({ today, tomorrow, upcoming, translate }) => {

  return (
    <>
      <Meta title={translate('events')} description={translate('eventsDescription')} />
      <EventsList items={today} sectionId={'today'} translateId={'today'} />

      <EventsList items={tomorrow} sectionId={'tomorrow'} translateId={'tomorrow'} />

      {!isEmpty(today) || !isEmpty(tomorrow) &&
        <>
          <div className='container grid-row padding-bottom-small'>
            <div className='grid-item item-s-12'><div className='border'></div></div>
          </div>

          {/*<section id='banner-ads'>
            <div className='container grid-row'>
              <div className='grid-item item-s-12'>
                <AdSense.Google
                  client='ca-pub-8460088592405505'
                  slot='4511576950'
                  style={{ display: 'block' }}
                  format='auto'
                  responsive='true'
                />
              </div>
            </div>
          </section>

          <div className='container grid-row padding-bottom-small'>
            <div className='grid-item item-s-12'><div className='border'></div></div>
          </div>*/}

          <DownloadAdContainer />
        </>
      }

      <EventsList items={upcoming} sectionId={'upcoming'} translateId={'upcoming'} />

      {/*!isEmpty(upcoming) &&
        <>
          <div className='container grid-row padding-bottom-small'>
            <div className='grid-item item-s-12'><div className='border'></div></div>
          </div>

          <section id='banner-ads'>
            <div className='container grid-row'>
              <div className='grid-item item-s-12'>
                <AdSense.Google
                  client='ca-pub-8460088592405505'
                  slot='4511576950'
                  style={{ display: 'block' }}
                  format='auto'
                  responsive='true'
                />
              </div>
            </div>
          </section>
        </>
      */}

      {isEmpty(today) && isEmpty(tomorrow) &&
        <>
          <div className='container grid-row padding-bottom-small'>
            <div className='grid-item item-s-12'><div className='border'></div></div>
          </div>

          <DownloadAdContainer />
        </>
      }
    </>
  )
}

export default withTranslate(Events)
