import React from 'react'
import { Link } from 'react-router-dom'
import HomeMobile from 'components/HomeMobile'
import ItemsContainer from 'containers/ItemsContainer'
import EventsList from 'components/EventsList'
import HomeFeatured from 'components/HomeFeatured'
import FeaturedArticlesList from 'components/FeaturedArticlesList'
import DownloadAdContainer from 'containers/DownloadAdContainer'
import { isMobileOnly } from 'react-device-detect'
import { nowDate, nowSeconds, oneWeekFromNowSeconds } from 'lib/dates'
import Meta from 'components/Meta'
import { withTranslate } from 'lib/translations'

const Home = ({ translate }) => {
  if (isMobileOnly) {
    return (
      <>
        <Meta title='Onda MX' description={translate('homeDescription')} />
        <HomeMobile />
      </>
    )
  }

  return (
    <>
      <Meta title='Onda MX' description={translate('homeDescription')} />
      <ItemsContainer
        Component={HomeFeatured}
        firestoreQuery={[
          {
            collection: 'events',
            orderBy: ['closing', 'desc'],
            where: [
              ['highlight','==','featured'],
              ['status','==','published'],
              ['closing', '>', nowDate()],
            ],
            storeAs: 'events-featured',
          },
        ]}
        sortCompare={(a, b) => {
          return a.featuredOrder - b.featuredOrder
        }}
      />
      <ItemsContainer
        Component={EventsList}
        firestoreQuery={[
          {
            collection: 'events',
            where: [
              ['highlight','==','highlighted'],
              ['status','==','published'],
              ['closing', '>', nowDate()],
            ],
            orderBy: ['closing', 'desc'],
            storeAs: 'events-highlight',
          }
        ]}
        sortCompare={(a, b) => {
          return b.openingStart.seconds - a.openingStart.seconds
        }}
        sectionId='highlights'
        translateId='highlights'
      />
      <ItemsContainer
        Component={EventsList}
        firestoreQuery={[
          {
            collection: 'events',
            where: [
              ['status','==','published'],
              ['closing', '>', nowDate()],
            ],
            storeAs: 'events',
          },
        ]}
        sortCompare={(a, b) => {
          return b.openingStart.seconds - a.openingStart.seconds
        }}
        filterCallback={(item) => {
          return item.openingStart.seconds < nowSeconds
          && item.highlight !== 'highlighted'
        }}
        limit={16}
        sectionId='whats-on'
        translateId='whatsOn'
      />
      <ItemsContainer
        Component={EventsList}
        firestoreQuery={[
          {
            collection: 'events',
            where: [
              ['status','==','published'],
              ['closing', '>', nowDate()],
            ],
            storeAs: 'events',
          },
        ]}
        filterCallback={(item) => {
          return item.openingStart.seconds < nowSeconds
          && item.closing.seconds > nowSeconds
          && item.closing.seconds < oneWeekFromNowSeconds
          && item.highlight !== 'highlighted'
        }}
        sortCompare={(a, b) => {
          return a.closing.seconds - b.closing.seconds
        }}
        limit={4}
        sectionId='closing-soon'
        translateId='closingSoon'
      />
      <ItemsContainer
        Component={FeaturedArticlesList}
        firestoreQuery={[
          {
            collection: 'articles',
            where: [
              ['status','==','published'],
            ],
            storeAs: 'articles-featured',
            orderBy: ['publishDate', 'desc'],
            limit: 3,
          },
        ]}
        translateId='magazine'
      />
      <div className='padding-top-small'>
        <DownloadAdContainer />
      </div>
    </>
  )
}

export default withTranslate(Home)
