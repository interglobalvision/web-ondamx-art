import React from 'react'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import ArticleItem from 'components/ArticleItem'
import FeaturedArticleItem from 'components/FeaturedArticleItem'
import ArticlesMobile from 'components/ArticlesMobile'
import DownloadAdContainer from 'containers/DownloadAdContainer'
import { isMobileOnly } from 'react-device-detect'
import Meta from 'components/Meta'
import { withTranslate } from 'lib/translations'

const Articles = ({ items, translate }) => {
  if (isMobileOnly) {
    return (
      <>
        <Meta title={translate('magazine')} description={translate('magazineDescription')} />
        <ArticlesMobile items={items} />
      </>
    )
  }

  if (!isLoaded(items)) {
    return null
  }

  if (isEmpty(items)) {
    return null
  }

  const featuredItems = items.slice(0, 4)
  const archiveItems = items.slice(4)

  return (
    <section id='articles'>
      <Meta title={translate('magazine')} description={translate('magazineDescription')} />
      {featuredItems.map((item, index)=> {
        if (index === 2) {
          return (
            <div key='download-ad'>
              <FeaturedArticleItem key={item.id} item={item} index={index} />
              <div className='margin-top-small'>
                <DownloadAdContainer />
              </div>
              <div className='container grid-row'>
                <div className='grid-item item-s-12'><div className='border'></div></div>
              </div>
            </div>
          )
        }

        return (
          <FeaturedArticleItem key={item.id} item={item} index={index} />
        )
      })}

      <div className='container'>
        <div className='grid-row margin-top-small'>
          {archiveItems.map((item, index)=> {
            return (
              <ArticleItem key={item.id} item={item} />
            )
          })}
        </div>
      </div>
    </section>
  )
}

export default withTranslate(Articles)
