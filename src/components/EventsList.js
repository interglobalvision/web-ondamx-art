import React from 'react'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { Link } from 'react-router-dom'
import SectionHeading from 'components/SectionHeading'
import EventItem from 'components/EventItem'

const EventsList = ({ items, sectionId = null, translateId }) => {
  if (!isLoaded(items) || isEmpty(items)) {
    return null
  }

  return (
    <section id={sectionId}>
      <div className='container'>
        <div className='grid-row'>
          {translateId &&
            <div className='grid-item item-s-12'>
              <SectionHeading translateId={translateId} />
            </div>
          }
          {!isEmpty(items) && (
            items.map(item => <EventItem key={item.id} item={item} />)
          )}
        </div>
      </div>
    </section>
  )
}

export default EventsList
