import React from 'react'
import { Translate } from 'react-localize-redux'

const ReadMore = () => {
  return (
    <span className='u-inline-block font-brown font-bold'><Translate id='readMore' /></span>
  )
}

export default ReadMore
