import React from 'react'
import { compose } from 'redux'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import { withRouter } from 'react-router-dom'

const Header = ({ activeLanguage, setActiveLanguage, location, history }) => {
  const setLanguage = (code) => {
    setActiveLanguage(code)
    localStorage.setItem('language', code)
  }

  const isInitialRoute = location.key === undefined
  const isHome = location.pathname === '/'
  const isExpos = location.pathname === '/exposiciones'
  const isEvents = location.pathname === '/eventos'
  const isArticles = location.pathname === '/revista'
  const isAbout = location.pathname === '/acerca-de'
  const isArticle = location.pathname.includes('/escrito/')

  return (
    <header id='header'>
      <div className={'only-mobile padding-top-small padding-bottom-small'}>
        <div className='container'>
          <div className={'grid-row align-items-center justify-start'}>
            {!isHome &&
              <div className='grid-item item-s-4'>
                <button onClick={() => {
                  if (isInitialRoute){
                    history.push('/')
                  } else {
                    history.goBack()
                  }
                }}><img id='nav-arrow-back' src='/navArrowLeft.png' alt='Back'/></button>
              </div>
            }
            <div className={'grid-item item-m-auto text-align-center ' + (isHome ? 'item-s-12' : 'item-s-4')}>
              <h1 className='u-visuallyhidden'>Onda MX</h1>
              <Link to='/'><img id='logo-header' src='/logo-onda-blue.png' alt='Onda MX logo'/></Link>
            </div>
          </div>
        </div>
      </div>

      <div className={'not-mobile padding-top-small padding-bottom-small ' + (isArticles || isArticle ? 'background-blue-light' : '')}>
        <div className='container'>
          <div className={'grid-row align-items-center justify-between'}>
            <div className={'grid-item item-m-auto text-align-center ' + (isHome ? 'item-s-12' : 'item-s-4')}>
              <h1 className='u-visuallyhidden'>Onda MX</h1>
              <Link to='/'><img id='logo-header' src='/logo-onda-blue.png' alt='Onda MX logo'/></Link>
            </div>
            <nav className='grid-row item-m-8 item-l-7 justify-between font-bold font-size-tiny'>
              <div className={isExpos ? 'grid-item active' : 'grid-item'}>
                <Link to='/exposiciones'>
                  <Translate id='exhibitions' />
                </Link>
              </div>
              <div className={isEvents ? 'grid-item active' : 'grid-item'}>
                <Link to='/eventos'>
                  <Translate id='events' />
                </Link>
              </div>
              <div className={isArticles ? 'grid-item active' : 'grid-item'}>
                <Link to='/revista'>
                  <Translate id='magazine' />
                </Link>
              </div>
              <div className={isAbout ? 'grid-item active' : 'grid-item'}>
                <Link to='/acerca-de'>
                  <Translate id='about' />
                </Link>
              </div>
              {activeLanguage !== undefined &&
                <div className='grid-item'>
                  {activeLanguage.code === 'en' ? (
                    <button onClick={() => setLanguage('es')}>ES</button>
                  ) : (
                    <button onClick={() => setLanguage('en')}>EN</button>
                  )}
                </div>
              }
            </nav>
          </div>
        </div>
      </div>
    </header>
  )
}

export default compose(
  withLocalize,
  withRouter,
)(Header)
