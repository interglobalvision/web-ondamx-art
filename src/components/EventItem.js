import React from 'react'
import { compose } from 'redux'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent, withTranslate } from 'lib/translations'
import { trimByChar, constructUrlSlug } from 'lib/utils'
import { getMediaThumb } from 'lib/images'
import { formatEventDates } from 'lib/dates'

const EventItem = ({ item, activeLanguage, translate }) => {
  const content = flattenLocalizedContent(item, activeLanguage.code)
  const contentState = JSON.parse(content.mainContent)
  const firstParagraph = contentState.blocks[0].text
  const thumbUrl = content.coverImage ? getMediaThumb(content.coverImage.mediaUrl, 581, 459) : null
  const urlSlug = constructUrlSlug(item.slug, content.name)

  return (
    <div key={content.id} className='grid-item item-s-12 item-m-4 item-l-3 margin-bottom-small'>
      <Link to={`evento/${urlSlug}-${item.id}`}>
        <div className='thumb-holder margin-bottom-micro' style={{
          backgroundImage: `url(${thumbUrl})`
        }}><img src={thumbUrl} alt={content.name} className='u-visuallyhidden'/></div>
        <div>
          <h3>{content.name}</h3>
          <div><span className='font-size-micro'>{content.space.name}</span></div>
          <div className='font-grey font-size-micro'>
            <span className='font-uppercase'><Translate id={content.type} /></span>
            <span className='font-bullet'> • </span>
            {formatEventDates(content, activeLanguage.code, translate)}
          </div>
        </div>
      </Link>
    </div>
  )
}

export default compose(
  withLocalize,
  withTranslate
)(EventItem)
