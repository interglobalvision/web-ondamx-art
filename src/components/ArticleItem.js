import React from 'react'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent } from 'lib/translations'
import { trimByChar, constructUrlSlug } from 'lib/utils'
import { getMediaThumb } from 'lib/images'
import ArticleDate from 'components/ArticleDate'
import { isMobileOnly } from 'react-device-detect'

const ArticleItem = ({ item, index, activeLanguage }) => {
  const content = flattenLocalizedContent(item, activeLanguage.code)
  const contentState = JSON.parse(content.mainContent)
  const firstParagraph = contentState.blocks[0].text
  const thumbUrl = content.coverImage ? getMediaThumb(content.coverImage.mediaUrl, 581, 459) : null
  const urlSlug = constructUrlSlug(item.slug, content.title)

  return (
    <div className={isMobileOnly ? 'grid-item item-s-12 item-m-4 item-l-3 margin-bottom-basic' : 'grid-item item-s-12 item-m-4 item-l-3 margin-bottom-small'} key={content.id}>
      <Link to={`/escrito/${urlSlug}-${item.id}`}>
        <img src={thumbUrl} alt={content.title}/>
        <div className='font-grey font-size-micro'>
          <span className='font-uppercase'><Translate id={content.type} /> </span>
          <span className='font-bullet'>•</span>
          <span> <ArticleDate timestamp={content.publishDate.seconds} currentLanguage={activeLanguage.code}/></span>
        </div>
        <h3 className='font-bold'>{content.title}</h3>
        <div><span className={isMobileOnly ? 'font-size-tiny font-bold' : 'font-size-tiny'}>{content.subtitle}</span></div>
        {isMobileOnly &&
          <div><span className='font-size-tiny'>{trimByChar(firstParagraph, 135)}</span></div>
        }
        <div><span className='font-size-micro'><Translate id='by' /> {content.author}</span></div>
      </Link>
    </div>
  )
}

export default withLocalize(ArticleItem)
