import React from 'react'
import { Translate } from 'react-localize-redux'
import { constructUrl } from 'lib/utils'

const MobileShare = ({ title, slug, type, itemId }) => {
  if (typeof navigator.share === 'undefined') {
    return null
  }

  const handleShare = () => {
    navigator.share({
      url: constructUrl(title, slug, type, itemId),
      title,
    })
    .then(() => { console.log('share success') })
    .catch((error) => { console.error(error) })
  }

  return (
    <button className='font-bold font-brown' onClick={() => {
      handleShare()
    }}><img className='share-icon' src='/share.png' alt='Share' /></button>
  )
}

export default MobileShare
