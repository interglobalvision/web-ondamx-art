import React, { useEffect, useState, useRef } from 'react'
import { compose } from 'redux'
import Swiper from 'react-id-swiper'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { Link } from 'react-router-dom'
import { Translate, withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent, withTranslate } from 'lib/translations'
import { trimByChar, constructUrlSlug } from 'lib/utils'
import { getMediaThumb } from 'lib/images'
import { formatEventDates } from 'lib/dates'
import ReadMore from 'components/ReadMore'

const HomeFeatured = ({ items, activeLanguage, translate }) => {
  const [isHovered, setIsHovered] = useState(true)
  const [showNavigation, setShowNavigation] = useState(false)
  let mousemoveTimeout = null
  let scheduled = false

  const handleMousemove = () => {
    setShowNavigation(true)
    if (!scheduled) {
      scheduled = true
      mousemoveTimeout = window.setTimeout(() => {
        if (!isHovered) {
          setShowNavigation(false)
        }
      }, 3000)
    }
  }

  useEffect(() => {
    window.addEventListener('mousemove', handleMousemove)
    return () => {
      window.removeEventListener('mousemove', handleMousemove)
      window.clearTimeout(mousemoveTimeout)
    }
  }, [isHovered])

  if (!isLoaded(items)) {
    return null
  }

  if (isEmpty(items)) {
    return null
  }

  const params = {
    loop: items.length > 1 ? true : false,
    simulateTouch: items.length > 1 ? true : false,
    navigation: items.length > 1 ? {
      nextEl: '.slider-button-next',
      prevEl: '.slider-button-prev',
    } : {},
    renderPrevButton: items.length > 1 ? () => <button className="slider-nav-button slider-button-prev"><img src='/navArrowLeft.png' /></button> : null,
    renderNextButton: items.length > 1 ? () => <button className="slider-nav-button slider-button-next"><img src='/navArrowRight.png' /></button> : null,
    pagination: items.length > 1 ? {
      el: '.slider-pagination',
      bulletElement: 'div',
      bulletClass: 'slider-pagination-bullet',
      bulletActiveClass: 'slider-pagination-bullet-active',
      type: 'bullets',
      clickable: true
    } : {},
  }

  return (
    <section
      className={'padding-bottom-small ' + (showNavigation ? 'show-navigation' : '')}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      <div className='container'>
        <Swiper {...params}>
          {items.map(item => {
            const content = flattenLocalizedContent(item, activeLanguage.code)
            const contentState = JSON.parse(content.mainContent)
            const firstParagraph = contentState.blocks[0].text
            const thumbUrl = content.coverImage ? getMediaThumb(content.coverImage.mediaUrl, 1005, 666) : null
            const urlSlug = constructUrlSlug(item.slug, content.name)

            return (
              <div key={content.id}>
                <Link to={`evento/${urlSlug}-${item.id}`}>
                  <div className='grid-row'>
                    <div className='grid-item item-s-7'>
                      <img src={thumbUrl} alt={content.name} />
                    </div>
                    <div className='grid-item item-s-5 grid-column justify-between'>
                      <div className='offset-s-3 font-heading font-grey margin-top-small'><Translate id={content.type} /></div>
                      <h3 className='font-bold font-size-large'>{content.name}</h3>
                      <div>
                        <div className='offset-s-3 font-bold font-size-mid margin-bottom-tiny'><span>{content.space.name}</span></div>
                        <div className='offset-s-3 font-grey margin-bottom-tiny'>
                          {formatEventDates(content, activeLanguage.code, translate)}
                        </div>
                        <div className='offset-s-3'>
                          {content.featuredSummary ? (
                            <span>{trimByChar(content.featuredSummary)} <ReadMore /></span>
                          ) : (
                            <span>{trimByChar(firstParagraph)} <ReadMore /></span>
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
            )
          })}
        </Swiper>
      </div>
    </section>
  )
}

export default compose(
  withLocalize,
  withTranslate
)(HomeFeatured)
