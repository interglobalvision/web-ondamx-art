import React from 'react'
import { Translate } from 'react-localize-redux'
import {
  isMobileOnly,
  isAndroid,
  isIOS
} from 'react-device-detect'

const OpenInApp = () => {
  if (!isMobileOnly || (!isAndroid && !isIOS)) {
    return null
  }

  return (
    <div id='open-in-app' className='background-grey-medium-light font-grey-dark padding-bottom-tiny padding-top-small'>
      <div className='container'>
        <div className='grid-row align-items-center'>
          <div className='grid-item'>
            <img id='open-in-app-logo' src='/logo-onda-bright.png' alt='Onda MX icon'/>
          </div>
          <div className='grid-item'>
            <span className='font-bold'>Onda MX</span><br />
            <span><Translate id={isIOS ? 'isFreeAppStore': 'isFreeGooglePlay'} /></span>
          </div>
          <div className='grid-item text-align-center flex-grow'>
            <a id='open-in-app-link' className='background-blue-bright' href={isIOS ? 'https://apps.apple.com/mx/app/onda-mx/id1473217717' : 'https://play.google.com/store/apps/details?id=com.interglobalvision.ondamx&hl=es'}><Translate id='view' /></a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default OpenInApp
