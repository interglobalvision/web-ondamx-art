import React from 'react'
import PropTypes from 'prop-types'
import { Route, Switch, Redirect } from 'react-router-dom'
import Home from 'components/Home'
import ArticleContainer from 'containers/ArticleContainer'
import EventContainer from 'containers/EventContainer'
import ArticlesContainer from 'containers/ArticlesContainer'
import ExhibitionsContainer from 'containers/ExhibitionsContainer'
import EventsContainer from 'containers/EventsContainer'
import FeaturedContainer from 'containers/FeaturedContainer'
import HighlightsContainer from 'containers/HighlightsContainer'
import OngoingExhibitionsContainer from 'containers/OngoingExhibitionsContainer'
import UpcomingExhibitionsContainer from 'containers/UpcomingExhibitionsContainer'
import UpcomingEventsContainer from 'containers/UpcomingEventsContainer'
import ClosingExhibitionsContainer from 'containers/ClosingExhibitionsContainer'
import AboutContainer from 'containers/AboutContainer'
import TermsContainer from 'containers/TermsContainer'
import PrivacyContainer from 'containers/PrivacyContainer'

const MainContent = () => {
  return (
    <main id='main-content'>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/escrito/(.*\-)?:itemId' component={ArticleContainer} />
        <Route exact path='/evento/(.*\-)?:itemId' component={EventContainer} />
        <Route exact path='/revista' component={ArticlesContainer} />
        <Route exact path='/exposiciones' component={ExhibitionsContainer} />
        <Route exact path='/eventos' component={EventsContainer} />
        <Route exact path='/especiales' component={FeaturedContainer} />
        <Route exact path='/destacados' component={HighlightsContainer} />
        <Route exact path='/actualmente' component={OngoingExhibitionsContainer} />
        <Route exact path='/proximas-exposiciones' component={UpcomingExhibitionsContainer} />
        <Route exact path='/proximos-eventos' component={UpcomingEventsContainer} />
        <Route exact path='/ultimos-dias' component={ClosingExhibitionsContainer} />
        <Route exact path='/acerca-de' component={AboutContainer} />
        <Route exact path='/terminos-y-condiciones' component={TermsContainer} />
        <Route exact path='/privacidad' component={PrivacyContainer} />
        <Route render={() => ( <Redirect to='/' /> )} />
      </Switch>
    </main>
  )
}

export default MainContent
