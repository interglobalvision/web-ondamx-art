import React, { useState } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { Translate } from 'react-localize-redux'
import { format } from 'date-fns'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { convertToTimeZone } from 'date-fns-timezone'
import { timeZone } from 'lib/dates'

const SpaceDetails = ({
  content,
  translate,
  showAddress,
  showHours,
  showPhone,
  showWebsite,
  showEmail,
  showInstagram,
  spaceContent,
}) => {

  const [isHoursOpen, setHoursOpen] = useState(false)

  const {
    location,
    hours,
  } = content

  let appointment = content.appointment === undefined ? 'default' : content.appointment

  const hoursOrder = [
    'monday',
    'tuesday',
    'wednesday',
    'thursday',
    'friday',
    'saturday',
    'sunday',
  ]
  let hoursArray = []
  let hasHours = appointment === 'only' ? true : false

  if (Object.keys(hours).length > 0) {
    hoursOrder.forEach(day => {
      if (hours[day][0] !== null && hours[day][1] !== null) {
        // assemble hoursArray with ordered hours
        hoursArray.push({
          id: day,
          times: hours[day],
        })
        hasHours = true
      }
    })
  }

  const renderAppointment = () => {
    switch (appointment) {
      case 'option':
        return (
          <span><Translate id='orAppointment' /></span>
        )
        break
      case 'only':
        return (
          <span><Translate id='onlyAppointment' /></span>
        )
        break
      case 'and':
        return (
          <span><Translate id='andAppointment' /></span>
        )
        break
      default:
        return null
    }
  }

  const renderAllHours = () => {
    if (appointment !== 'only') {
      return (
        hoursArray.map(day => {
          const openDate = convertToTimeZone(new Date(day['times'][0].seconds * 1000), { timeZone})
          const closeDate = convertToTimeZone(new Date(day['times'][1].seconds * 1000), { timeZone})
          return(
            <div className='grid-row justify-between' key={day['id']}>
              <span>{translate(day['id'])}&nbsp;</span>
              <span>{format(openDate, 'HH:mm')} — {format(closeDate, 'HH:mm')}</span>
            </div>
          )
        })
      )
    }
    return null
  }

  const renderTodayHours = () => {
    const now = convertToTimeZone(new Date(), { timeZone})
    // index returned by getDay() is based on a week
    // that starts with Sunday, so we adjust for that here
    const todayIndex = now.getDay() === 0 ? 6 : now.getDay() - 1
    const todayHours = hours[hoursOrder[todayIndex]]

    if (appointment === 'only') {
      return <span><Translate id='onlyAppointment' /></span>
    }

    if (todayHours[0] !== null && todayHours[1] !== null) {
      // space open today
      // form today string with today's hours
      const openDate = convertToTimeZone(new Date(todayHours[0].seconds * 1000), { timeZone})
      const closeDate = convertToTimeZone(new Date(todayHours[1].seconds * 1000), { timeZone})

      return (
        <div className='grid-row justify-between'>
          <span>{translate('today').toLowerCase()} {translate('open')}</span>
          <span>{format(openDate, 'HH:mm')} — {format(closeDate, 'HH:mm')}</span>
        </div>
      )
    } else {
      return (
        <div>
          <span>{translate('closed')} {translate('today').toLowerCase()}</span>
        </div>
      )
    }
    return null
  }

  const renderAddress = () => {

    if (!showAddress || location.address === undefined) {
      return null
    }

    if (!location.address.street.length) {
      return null
    }

    const { lat, lon } = location
    const hasLatLon = lat !== 0 && lon !== 0

    const {
      street,
      number,
      extra,
      neighborhood,
    } = location.address

    return (
      <div className='grid-row flex-nowrap margin-bottom-micro'>
        <div className='details-icon-holder'>
          <img src='/detailsLocation.png' className='details-icon details-location' />
        </div>
        <div>
          <div>
            <span>{street} {number}</span>
            <span>{extra.length ? `, ${extra}` : '' }</span>
          </div>
          <div>
            <span>{neighborhood}</span>
          </div>
        </div>
      </div>
    )
  }

  const renderPhone = () => {
    if (showPhone && isLoaded(spaceContent) && !isEmpty(spaceContent)) {
      const { phone } = spaceContent
      if (phone !== undefined) {
        if (phone.length > 0) {
          return (
            <div className='grid-row flex-nowrap margin-bottom-micro'>
              <div className='details-icon-holder'>
                <img src='/detailsPhone.png' className='details-icon details-phone' />
              </div>
              <div>
                <a href={`tel:${phone}`}>{phone}</a>
              </div>
            </div>
          )
        }
      }
      return null
    }
  }

  const renderWebsite = () => {
    if (showWebsite && isLoaded(spaceContent) && !isEmpty(spaceContent)) {
      const { website } = spaceContent
      if (website !== undefined) {
        if (website.length > 0) {
          return (
            <div className='grid-row flex-nowrap margin-bottom-micro'>
              <div className='details-icon-holder'>
                <img src='/detailsLink.png' className='details-icon details-link' />
              </div>
              <div>
                <a href={website}>{website}</a>
              </div>
            </div>
          )
        }
      }
      return null
    }
  }

  const renderEmail = () => {
    if (showEmail && isLoaded(spaceContent) && !isEmpty(spaceContent)) {
      const { email } = spaceContent
      if (email !== undefined) {
        if (email.length > 0) {
          return (
            <div className='grid-row flex-nowrap margin-bottom-micro'>
              <div className='details-icon-holder'>
                <img src='/detailsEmail.png' className='details-icon details-email' />
              </div>
              <div>
                <a href={`mailto:${email}`}>{email}</a>
              </div>
            </div>
          )
        }
      }
      return null
    }
  }

  const renderInstagram = () => {
    if (showInstagram && isLoaded(spaceContent) && !isEmpty(spaceContent)) {
      const { instagram } = spaceContent
      if (instagram !== undefined) {
        if (instagram.length > 0) {
          return (
            <div className='grid-row flex-nowrap margin-bottom-micro'>
              <div className='details-icon-holder'>
                <img src='/detailsInstagram.png' className='details-icon details-instagram' />
              </div>
              <div>
                <a href={`https://instagram.com/${instagram}`}>{instagram}</a>
              </div>
            </div>
          )
        }
      }
      return null
    }
  }

  return (
    <div id='space-details'>
      {renderAddress()}
      {(showHours && hasHours) &&
        <div className='grid-row u-pointer flex-nowrap margin-bottom-micro' onClick={() => {setHoursOpen(!isHoursOpen)}}>
          <div className='details-icon-holder'>
            <img src='/detailsHours.png' className='details-icon details-hours' />
          </div>
          <div className='grid-row'>
            <div>
              {isHoursOpen ? (
                <div>
                  {renderAllHours()}
                  {renderAppointment()}
                </div>
              ) : (
                <div>
                  {renderTodayHours()}
                  {appointment === 'and' &&
                    <div>
                      {renderAppointment()}
                    </div>
                  }
                </div>
              )}
            </div>
            {appointment !== 'only' &&
              <div>
                <img src='/accordionToggle.png' className={isHoursOpen ? 'accordion-toggle open' : 'accordion-toggle'} />
              </div>
            }
          </div>
        </div>
      }
      {renderPhone()}
      {renderWebsite()}
      {renderEmail()}
      {renderInstagram()}
    </div>
  )
}

const mapStateToProps = (state, props) => {
  const { id } = props.space

  return {
    spaceContent: state.firestore.data[`space-${id}`],
  }
}

export default compose(
  firestoreConnect(props => {
    const { id } = props.space

    return [{
      collection: 'spaces',
      doc: id,
      storeAs: `space-${id}`
    }]
  }),
  connect(mapStateToProps)
)(SpaceDetails)
