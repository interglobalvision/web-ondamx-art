import React from 'react'
import { compose } from 'redux'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent } from 'lib/translations'
import { isMobileOnly } from 'react-device-detect'
import { getMediaThumb } from 'lib/images'

const DownloadAd = ({ settings, activeLanguage }) => {

  if (!isLoaded(settings)) {
    return null
  }

  if (isEmpty(settings)) {
    return null
  }

  const content = flattenLocalizedContent(settings[0], activeLanguage.code)

  const downloadAd = isMobileOnly ? content.downloadAdMobile : content.downloadAdDesktop
  const width = isMobileOnly ? 375 : 940
  const height = isMobileOnly ? 180 : 360

  if (downloadAd === undefined) {
    return null
  }

  if (downloadAd !== null) {
    var regex = /(?:\.([^.]+))?$/
    var extension = regex.exec(downloadAd.metadata.name)[1]
    //const thumbUrl = extension === 'gif' ? downloadAd.mediaUrl : getMediaThumb(downloadAd.mediaUrl, width, height)
    const thumbUrl = downloadAd.mediaUrl

    return (
      <section id='download-ad' className='text-align-center'>
        <div className={isMobileOnly ? 'grid-row' : 'grid-row margin-bottom-small'}>
          <div className='grid-item item-s-12 no-gutter'>
            <img src={thumbUrl} />
          </div>
        </div>
      </section>
    )
  }

  return null
}

export default compose(
  withLocalize,
)(DownloadAd)
