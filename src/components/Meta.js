import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { compose } from 'redux'
import { firestoreConnect } from 'react-redux-firebase'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { Helmet } from 'react-helmet'
import { withLocalize } from 'react-localize-redux'
import { withRouter } from 'react-router-dom'
import { getMediaThumb } from 'lib/images'

const BASE_URL = 'https://ondamx.art/'

const Meta = ({
  title = '',
  description = '',
  type = 'website',
  image = false,
  robots = 'index',
  activeLanguage = { code: 'es_MX' },
  location,
  settings
}) => {

  let imageUrl = ''
  let imageWidth = null
  let imageHeight = null

  if (image) {
    imageUrl = image.mediaUrl
    imageWidth = image.width
    imageHeight = image.height
  } else if (isLoaded(settings) && !isEmpty(settings)) {
    if (settings[0].defaultOgImage) {
      imageUrl = settings[0].defaultOgImage.mediaUrl
      imageWidth = settings[0].defaultOgImage.width
      imageHeight = settings[0].defaultOgImage.height
    }
  }

  return (
    <Helmet>
      <meta charSet='utf-8' />
      <title>{title} | Onda MX</title>
      <meta name='description' content={description} />
      <meta name='robots' content={robots} />
      <meta property='og:title' content={title} />
      <meta property='og:url' content={`${BASE_URL}${location.pathname}`} />
      <meta property='og:type' content={type} />
      <meta property='og:description' content={description} />
      <meta property='og:locale' content={activeLanguage.code === 'en' ? 'en_US' : 'es_LA'} />
      <meta property='og:locale:alternate' content={activeLanguage.code === 'en' ? 'es_LA' : 'en_US'} />
      <meta property='og:site_name' content='Onda MX' />
      <meta property='og:image' content={imageUrl} />
      {imageWidth !== null &&
        <meta property='og:image:width' content={imageWidth} />
      }
      {imageHeight !== null &&
        <meta property='og:image:height' content={imageHeight} />
      }
    </Helmet>
  )
}

Meta.propTypes = {
  settings: PropTypes.array,
}

const mapStateToProps = (state, props) => {
  return {
    settings: state.firestore.ordered['settings-web']
  }
}

export default compose(
  withLocalize,
  withRouter,
  firestoreConnect([
    {
      collection: 'settings',
      doc: 'web',
      storeAs: 'settings-web',
    }
  ]),
  connect(mapStateToProps)
)(Meta)
