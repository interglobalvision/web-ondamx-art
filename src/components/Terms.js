import React from 'react'
import { compose } from 'redux'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import { withLocalize } from 'react-localize-redux'
import { flattenLocalizedContent, withTranslate } from 'lib/translations'
import { getDraftHtml } from 'lib/utils'
import SectionHeading from 'components/SectionHeading'
import { Link } from 'react-router-dom'
import Meta from 'components/Meta'

const Terms = ({ settings, activeLanguage, translate }) => {
  if (!isLoaded(settings)) {
    return null
  }

  if (isEmpty(settings)) {
    return null
  }

  const content = flattenLocalizedContent(settings[0], activeLanguage.code)
  const termsContent = getDraftHtml(settings[0], activeLanguage.code, 'termsContent')

  return (
    <section id='terms'>
      <Meta
        title={translate('terms')}
      />
      <div className='container'>
        <div className='grid-row margin-bottom-basic'>
          <div className='grid-item item-s-12'>
            <SectionHeading translateId={'terms'} />
          </div>
          <div className='grid-item item-s-12 item-m-10 item-l-8'>
            <div className='margin-bottom-small' dangerouslySetInnerHTML={{__html: termsContent.contentHtml }} />
          </div>
        </div>
      </div>
    </section>
  )
}

export default compose(
  withLocalize,
  withTranslate
)(Terms)
