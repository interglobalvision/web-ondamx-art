import React from 'react'
import { Link } from 'react-router-dom'
import { isLoaded, isEmpty } from 'react-redux-firebase'
import FeaturedArticleItem from 'components/FeaturedArticleItem'
import { Translate } from 'react-localize-redux'

const FeaturedArticlesList = ({ items, location }) => {
  if (!isLoaded(items)) {
    return null
  }

  if (isEmpty(items)) {
    return null
  }

  return (
    <section id='featured-articles' className='background-blue-light'>
      <div className='container'>
        <div className='grid-row'>
          <div className='grid-item item-s-12'>
            <h2 className='padding-top-small font-heading font-size-large'><Translate id={'magazine'} /></h2>
          </div>
        </div>
      </div>
      {items.map((item, index)=> <FeaturedArticleItem key={item.id} item={item} index={index} />)}
      <div className='container'>
        <div className='grid-row'>
          <div className='grid-item item-s-12 text-align-center padding-top-small padding-bottom-small font-brown font-size-mid'>
            <Link to={`/revista`} className='font-bold'><Translate id={'viewAll'} /></Link>
          </div>
        </div>
      </div>
    </section>
  )
}

export default FeaturedArticlesList
