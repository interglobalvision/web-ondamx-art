import { connect } from 'react-redux'
import { getTranslate } from 'react-localize-redux'

export const withTranslate = (WrappedComponent) => {
  return connect((state, props) => {
    return {
      translate: getTranslate(state.localize)
    }
  })(WrappedComponent)
}

export const getDefaultLanguageCode = () => {
  const browserLanguage = window.navigator.userLanguage || window.navigator.language
  const storageLanguage = localStorage.getItem('language')

  if (storageLanguage) {
    return storageLanguage
  }

  if (browserLanguage.substring(0,2) === 'en') {
    return 'en'
  }

  return 'es'
}

export const flattenLocalizedContent = (content, language) => {
  if (content.localizedContent !== undefined && content.localizedContent[language] !== undefined) {
    // https://codeburst.io/use-es2015-object-rest-operator-to-omit-properties-38a3ecffe90
    const {
      localizedContent,
      ...otherContent
    } = content

    return {
      ...otherContent,
      ...localizedContent[language]
    }
  }

  else if (content[language] !== undefined) {
    return content[language]
  }

  return content
}

export const getAvailableLanguage = (languages, languageCode) => {
  let availability = {
    available: [],
    missing: []
  }

  Object.keys(languages).forEach(key => {
    if (languages[key]) {
      availability.available.push(key)
    } else {
      availability.missing.push(key)
    }
  })

  if (availability.available.length === 0) {
    return false
  }

  if (availability.missing.includes(languageCode)) {
    languageCode = availability.available[0]
  }

  return languageCode
}
