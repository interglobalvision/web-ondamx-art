import React from 'react'
import { convertFromRaw } from 'draft-js'
import { convertToHTML } from 'draft-convert'
import { flattenLocalizedContent } from 'lib/translations'
import { getAvailableLanguage } from 'lib/translations'

export const constructUrlSlug = (slug, title) => {
  const urlSlug = slug !== undefined ? slug : title.replace(/[^a-zA-Z0-9]/g,'-').toLowerCase()
  return urlSlug
}

export const constructUrl = (title, slug, type, itemId) => {
  const urlSlug = constructUrlSlug(slug, title)
  return `https://ondamx.art/${type}/${urlSlug}-${itemId}`
}

export const trimByWord = (str, maxWords = 35) => {
  let result = str
  let resultArray = result.split(' ')

  if (resultArray.length > maxWords) {
    resultArray = resultArray.slice(0, maxWords)
    result = resultArray.join(' ') + '…'
  }

  return result
}

export const trimByChar = (str, maxChars = 180) => {
  let result = str
  let resultArray = result.split('')

  if (resultArray.length > maxChars) {
    resultArray = resultArray.slice(0, maxChars)
    result = resultArray.join('') + '…'
  }

  return result
}

export const getDraftHtml = (item, activeLanguageCode = 'es', contentKey = 'mainContent') => {
  const availableLanguageCode = getAvailableLanguage(item.languages, activeLanguageCode)
  const localizedContent = item.localizedContent[availableLanguageCode]
  const parsedContent = JSON.parse(localizedContent[contentKey])
  const contentState = convertFromRaw(parsedContent)

  const contentHtml = convertToHTML({
    entityToHTML: (entity, originalText) => {
      switch (entity.type) {
        case 'IMAGE':
          const content = flattenLocalizedContent(entity.data, availableLanguageCode)

          let altText = ''

          if (content.caption.length > 0) {
            // has caption
            altText = content.caption
          } else if (localizedContent.title !== undefined) {
            // is article
            altText = localizedContent.title
          } else if (localizedContent.name !== undefined) {
            // is event
            altText = localizedContent.name
          }

          return (
            <figure className='inline-image'>
              <img src={content.src} alt={altText}/>
              {content.caption.length > 0 &&
                <figcaption className='font-grey font-size-micro'>{content.caption}</figcaption>
              }
            </figure>
          )
        case 'LINK':
          return (
            <a href={entity.data.url}>{originalText}</a>
          )
        case 'draft-js-video-plugin-video':
          const { src } = entity.data
          const videoId = src.match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=))([\w\-]{10,12})\b/)[1]
          return (
            <div className='video-embed-container'>
              <iframe src={`https://www.youtube.com/embed/${videoId}`} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen></iframe>
            </div>
          )
        default:
          return null
      }
    }
  })(contentState)

  return {
    contentHtml,
    isSameLanguage: availableLanguageCode === activeLanguageCode
  }
}
