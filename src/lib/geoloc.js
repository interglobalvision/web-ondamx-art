const googleMapsApiKey = 'AIzaSyBUQnKGSCeMZJHyQJQzNhYoLTTUirJrk7U'

export const getStaticMapUrl = (lat, lon, width = 640, height = 640, zoom = 15, marker = false, markerColor = 'black') => {
  let url = 'https://maps.googleapis.com/maps/api/staticmap?'
  url += 'center=' + lat + ',' + lon
  url += '&zoom=' + zoom
  url += marker ? '&markers=color:' + markerColor + '%7C' + lat + ',' + lon : ''
  url += '&size=' + width + 'x' + height
  url += '&scale=2'
  url += '&key=' + googleMapsApiKey

  return url
}
