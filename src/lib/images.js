/*
[width, height],
[1005,1260],
[1005,666],
[940,360],
[880,309],
[581,459],
[375,180],
[324,324],
[68,68],
*/

export const getMediaThumb = (url, width, height) => {
  const filename = url.substring(url.lastIndexOf('/')+1);
  const parts = filename.split('.')
  const thumbFilename = `${parts[0]}_${width}x${height}_thumb.${parts[1]}`

  return url.replace(filename,thumbFilename)
}
