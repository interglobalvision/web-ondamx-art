import { createStore } from 'redux'
import { applyMiddleware, compose } from 'redux'
import window from 'global/window'
import { createStoreWithFirebase } from 'lib/firebase'
import { getFirebase } from 'react-redux-firebase'
import thunk from 'redux-thunk'
import rootReducer from 'reducers'

const middleware = [thunk.withExtraArgument(getFirebase)] // add middlewares

const composedEnhancers = compose(
  applyMiddleware(...middleware),
)

function configureStore(preloadedState = {}) {
  return createStoreWithFirebase(
    rootReducer,
    preloadedState,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
  )
}

export default configureStore
