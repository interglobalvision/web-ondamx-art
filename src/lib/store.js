import { applyMiddleware, compose } from 'redux'
import rootReducer from 'reducers'
import thunk from 'redux-thunk'
import { createStoreWithFirebase } from 'lib/firebase'
import { getFirebase } from 'react-redux-firebase'

// Grab the state from a global variable injected into the server-generated HTML
const preloadedState = window.__PRELOADED_STATE__
// Allow the passed state to be garbage-collected
delete window.__PRELOADED_STATE__

const initialState = {...preloadedState}
const enhancers = []
const middleware = [thunk.withExtraArgument(getFirebase)] // add middlewares

// This is boilerplate code from the redux devtools extension
if (process.env.NODE_ENV === 'development') {
  const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__

  if (typeof devToolsExtension === 'function') {
    enhancers.push(devToolsExtension())
  }
}

const composedEnhancers = compose(
  applyMiddleware(...middleware),
  ...enhancers
)

const store = createStoreWithFirebase(
  rootReducer,
  initialState,
  composedEnhancers
)

export default store
