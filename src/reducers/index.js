import { combineReducers } from 'redux'
import { firebaseReducer } from 'react-redux-firebase'
import { firestoreReducer } from 'redux-firestore'
import { localizeReducer } from 'react-localize-redux'

// Reducers
const appReducer = combineReducers({
  firebase: firebaseReducer,
  firestore: firestoreReducer,
  localize: localizeReducer,
})

// Setup root reducer
const rootReducer = (state, action) => {
  const newState = (action.type === 'RESET') ? undefined : state
  return appReducer(newState, action)
}

export default rootReducer
