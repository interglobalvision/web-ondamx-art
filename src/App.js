import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { Route, StaticRouter } from 'react-router-dom'
import { compose } from 'redux'
import { withLocalize } from 'react-localize-redux'

import englishTranslations from 'translations/en.translations.json'
import spanishTranslations from 'translations/es.translations.json'
import { getDefaultLanguageCode } from 'lib/translations'

import MainContainer from 'containers/MainContainer'
import Header from 'components/Header'
import Footer from 'components/Footer'
import OpenInApp from 'components/OpenInApp'
import 'static/css/main.css'

class App extends React.Component {
  componentWillMount() {
    this.props.initialize({
      languages: [
        { name: "Español", code: "es" },
        { name: "English", code: "en" }
      ],
      options: {
        renderToStaticMarkup: ReactDOMServer.renderToStaticMarkup,
      }
    })

    this.props.addTranslationForLanguage(spanishTranslations, 'es')
    this.props.addTranslationForLanguage(englishTranslations, 'en')
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      this.props.setActiveLanguage(getDefaultLanguageCode())
    }
  }

  render() {
    return (
      <div className='App'>
        <OpenInApp />
        <Header/>
        <MainContainer/>
        <Footer />
      </div>
    )
  }
}

export default compose(
  withLocalize,
)(App)
